"""
If you need to split your dataset in train / test for evaluation
And put X_test CHURN column in y_test
"""
from os import path

import pandas as pd
from sklearn.model_selection import train_test_split

import src.settings.settings as stg

# Import dataset
customers_path = stg.CUSTOMERS_FILENAME
indicators_path = stg.INDICATORS_FILENAME

customers = pd.read_csv(customers_path, sep=stg.CUSTOMERS_SEP)
indicators = pd.read_csv(indicators_path, sep=stg.INDICATORS_SEP)

# Split dataset
customers_train, customers_test = train_test_split(customers, test_size=0.2, random_state=26)
indicators_train, indicators_test = train_test_split(indicators, test_size=0.2, random_state=26)

# Separate CHURN column from X_test. Save it in y_test
y_test = indicators_test[stg.CHURN]
indicators_test = indicators_test.drop(columns=[stg.CHURN])

# Export dataset
customers_train.to_csv(path.abspath(path.join(stg.TEST_DATA_DIR, 'customers_train.csv')), sep=stg.CUSTOMERS_SEP, index=False)
indicators_train.to_csv(path.abspath(path.join(stg.TEST_DATA_DIR, 'indicators_train.csv')), sep=stg.INDICATORS_SEP, index=False)

customers_test.to_csv(path.abspath(path.join(stg.TEST_DATA_DIR, 'customers_test.csv')), sep=stg.CUSTOMERS_SEP, index=False)
indicators_test.to_csv(path.abspath(path.join(stg.TEST_DATA_DIR, 'indicators_test.csv')), sep=stg.INDICATORS_SEP, index=False)
y_test.to_csv(path.abspath(path.join(stg.TEST_DATA_DIR, 'y_test.csv')), index=False)

# Print
print("=====DATASETS SIZES=====")
print("customers", customers.shape)
print("indicators", indicators.shape)

print("customers_train", customers_train.shape)
print("indicators_train", indicators_train.shape)

print("customers_test", customers_test.shape)
print("indicators_test", indicators_test.shape)
print("y_test", y_test.shape)
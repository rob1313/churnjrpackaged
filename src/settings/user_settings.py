from os import path

# Paths
REPO_DIR = path.abspath(path.join(path.dirname(__file__), '../../'))
DATA_DIR = path.join(REPO_DIR, 'data')
TEST_DATA_DIR = path.join(DATA_DIR, 'test')

# Enter filenames :
CUSTOMERS_TEST_FILENAME = 'customers_test.csv'
INDICATORS_TEST_FILENAME = 'indicators_test.csv'
LABELS_TEST_FILENAME = 'y_test.csv'

# Enter separator for your files :
CUSTOMERS_TEST_SEPARATOR = ";"
INDICATORS_TEST_SEPARATOR = ";"

# Paths to test datasets
CUSTOMERS_TEST_FILEPATH = path.abspath(path.join(TEST_DATA_DIR, CUSTOMERS_TEST_FILENAME))
INDICATORS_TEST_FILEPATH = path.abspath(path.join(TEST_DATA_DIR, INDICATORS_TEST_FILENAME))

if __name__ == "__main__":
    print("Train data should be stored in:")
    print(CUSTOMERS_TRAIN_FILENAME)
    print(INDICATORS_TRAIN_FILENAME)
    print("\nTest data should be stored in:")
    print(path.abspath(path.join(stg.TEST_DATA_DIR, CUSTOMERS_TEST_FILENAME)))
    print(path.abspath(path.join(stg.TEST_DATA_DIR, INDICATORS_TEST_FILENAME)))


# Yotta Project 1 - Advanced ML - Churn modelling

Copyright : 2020, Jerome Assouline, Robin Timsit

==============================

# Getting started

## 0. Clone this repository

```
$ git clone https://gitlab.com/yotta-academy/mle-bootcamp/projects/ml-project/fall-2020/churnjr.git
$ cd churnjr
```

## 1. Setup your virtual environment and activate it

Goal : create a local virtual environment in the folder `./.venv/`.

- First: check your python3 version:

    ```
    $ python3 --version
    # examples of outputs:
    Python 3.6.2 :: Anaconda, Inc.
    Python 3.7.2

    $ which python3
    /Users/benjamin/anaconda3/bin/python3
    /usr/bin/python3
    ```

    - If you don't have python3 and you are working on your mac: install it from [python.org](https://www.python.org/downloads/)
    - If you don't have python3 and are working on an ubuntu-like system: install from package manager:

        ```
        $ apt-get update
        $ apt-get -y install python3 python3-pip python3-venv
        ```

- Now that python3 is installed create your environment, activate it and install necessary packages:

    ```
    $ source init.sh
    ```

    You sould **allways** activate your environment when working on the project.

    If it fails with one of the following message :
    ```
    "ERROR: failed to create the .venv : do it yourself!"
    ```
    
- If you wish to do more than use the prediction script, install dev packages:

    ```
    $ poetry install #if you use poetry
    $ pip install -r requirements_dev.txt #if you don't have poetry
    ```

## 2. Place your data in the right folder

- Now you have installed all your packages you need to put your data for training in directory :
    ```
    churnjr/data/train/
    ```
- Put your the data you want to test in data/test/ 
    ```
    churnjr/data/test/
    ```
- If you have potential labels matching your test data, put it also in : 
    ```
    churnjr/data/test/
    ```

## 2bis. Download data

If you don't have data, you can download train and test data using :
    
    ```
    $ source download_data.sh
    ```

## 3. Check your settings
- Data filenames should be called :
    ```
    churnjr/data/train/customers.csv
    churnjr/data/train/indicators.csv
    
    churnjr/data/test/customers_test.csv
    churnjr/data/test/indicators_test.csv
    churnjr/data/test/y_test.csv
    ```
- But you can update your settings such as these filenames in the directory :
    ```
    churnjr/src/settings/user-settings.py
    ```

## 4. Get ready to predict !

- Now you have installed all your package you need to put your data for training in /data/train/
- Put your the data you want to test in data/test/ 

- Run this command : 
    ```
    $ source predict.sh
    ```

## 5. If you feel like updating source code & settings !

Your code will go in the folder `src/`.

You can change your settings (where data is stored, the names of features, the parameters of models...)
in `src/settings/`:
    - `settings.py` should contain the configuration and env. variables you can change

## 6. Module documentation

- To generate the module documentation:

    ```
    $ cd docs
    $ make html
    ```
- Explore documentation:
    ```
    $ open build/html/index.html 
    ```


# Project Organization
----------------------

    ├── activate.sh
    ├── init.sh
    ├── install_packages.sh
    ├── download_data.sh
    ├── README.md          <- The top-level README for users and developers using this project.
    ├── data
    │   ├── prediction       <- Output data.
    │   ├── test        <- Test data.
    │   └── train            <- The original, immutable data.
    │
    ├── docs
    │   ├── make.bat
    │   ├── Makefile
    │   ├── source        <- Files to build documentation.
    │   └── build
    │       ├── toctrees
    │       └── html       <- HTML files to see the module documentation.
    │
    │
    ├── models             <- Trained model.
    │
    ├── notebooks
    │   ├── churn_explo       <- EDA notebook.
    │   ├── churn_interpretability        <- Interpretability notebook.
    │   └── churn_model_optimization            <- Model optimization notebook.
    │
    │
    │
    ├── pyproject.toml            <- Requirements for developpers (needs poetry).  
    ├── requirements.txt            <- Requirements for users.
    ├── requirements_dev.txt            <- Requirements for developpers. 
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── infrastructure           <- Scripts to load and clean data.
    │   │   ├── raw_bank_data.py
    │   │   └── tecnical_cleaning.py
    │   │
    │   ├── settings       <- User and developpers settings
    │   │   ├── settings.py
    │   │   └── user_settings.py
    │   │
    │   ├── domain         <- Scripts to clean and create features and train model
    │   │   ├── domain_cleaning.py
    │   │   ├── train_model.py
    │   │   └──feature_engineering.py
    │   │
    │   └── application  <- Prediction script
    │       └── predict_model.py


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

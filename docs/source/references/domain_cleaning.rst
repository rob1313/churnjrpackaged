.. _domaincleaningmodule: domaincleaning

Module domain_cleaning
**********************
This document describes all classes of the module domain_cleaning.

.. automodule:: src.domain.domain_cleaning

============================================================
Class
============================================================

.. autoclass:: src.domain.domain_cleaning.FrequencyEncoder()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.domain_cleaning.FrequencyEncoder.transform()
    .. automethod:: src.domain.domain_cleaning.FrequencyEncoder.fit()

============================================================
Class
============================================================

.. autoclass:: src.domain.domain_cleaning.CustomOneHotEncoder()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.domain_cleaning.CustomOneHotEncoder.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.domain_cleaning.AberrantAgeImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.domain_cleaning.AberrantAgeImputer.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.domain_cleaning.AberrantNbProduitsImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.domain_cleaning.AberrantNbProduitsImputer.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.domain_cleaning.CreditScoreImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.domain_cleaning.CreditScoreImputer.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.domain_cleaning.SalaryImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.domain_cleaning.SalaryImputer.transform()

============================================================
Class
============================================================

.. autoclass:: src.domain.domain_cleaning.BalanceImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: src.domain.domain_cleaning.BalanceImputer.transform()
    .. automethod:: src.domain.domain_cleaning.BalanceImputer.fit()

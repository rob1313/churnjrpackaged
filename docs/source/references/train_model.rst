.. _trainmodelmodule: trainmodel

Module train_model
******************
This document describes all functions of the module train_model.

.. automodule:: src.domain.train_model

============================================================
Functions
============================================================

.. autofunction:: src.domain.train_model.train_model()
.. autofunction:: src.domain.train_model.evaluate_model()
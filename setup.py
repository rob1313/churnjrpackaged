from setuptools import find_packages, setup

setup(
    name='ChurnJR Packaged',
    packages=find_packages(),
    version='1.0',
    description='Churn Modelling',
    author='Jerome & Robin',
    license='',
)
